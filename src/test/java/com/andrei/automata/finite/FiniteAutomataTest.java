package com.andrei.automata.finite;

import com.andrei.automata.listeners.PrintToConsole;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.*;
import java.util.Collections;

/**
 * DFA Automata Tests
 */
public class FiniteAutomataTest {

    @Test
    public void DFATest() throws IOException, URISyntaxException {
        Path inputPath = Paths.get(this.getClass().getResource("/input").toURI());
        FiniteAutomata<String> automata = new FiniteAutomata<>(inputPath.toFile());

        if (automata.isAccessibleFile()) {
            PrintToConsole printToConsole = new PrintToConsole();

            automata.addObserver(printToConsole);
            automata.build();

            Assert.assertTrue(automata.isAcceptedInput("ab"));
            Assert.assertTrue(automata.isAcceptedInput("aba"));
            Assert.assertTrue(automata.isAcceptedInput("abc"));
            Assert.assertFalse(automata.isAcceptedInput("abd"));
        }
    }

    @Test
    public void InvalidStateTest() throws URISyntaxException {
        Path inputPath = Paths.get(this.getClass().getResource("/input").toURI());
        FiniteAutomata<String> automata = new FiniteAutomata<>(inputPath.toFile());

        FiniteState<String> invalidState = new FiniteState<>("Invalid", false, false);

        Assert.assertEquals(invalidState, automata.getState(null));
    }

    @Test
    public void inputAccesibleTest() throws IOException, URISyntaxException {
        Path trueInputPath = Paths.get(this.getClass().getResource("/input").toURI());
        FiniteAutomata<String> automataTrueInput = new FiniteAutomata<>(trueInputPath.toFile());

        Assert.assertTrue(automataTrueInput.isAccessibleFile());
    }

    @Test(expected = NullPointerException.class)
    public void inputNullTest() throws NullPointerException, IOException {
        FiniteAutomata automataNullInput = new FiniteAutomata(null);

        automataNullInput.isAccessibleFile();
    }

    @Test
    public void printFiniteAutomataTest() throws IOException, URISyntaxException {
        Path inputPath = Paths.get(this.getClass().getResource("/input").toURI());
        FiniteAutomata<String> automata = new FiniteAutomata<>(inputPath.toFile());

        if (automata.isAccessibleFile()) {
            PrintToConsole printToConsole = new PrintToConsole();

            automata.addObserver(printToConsole);
            automata.build();
        }

        automata.notifyObservers(automata);
    }

    @Test(expected = IOException.class)
    public void inputInaccessibleTest() throws IOException, URISyntaxException {

        Path file = Paths.get(this.getClass().getResource("/fakeInput").toURI());
        AclFileAttributeView aclAttr = Files.getFileAttributeView(file, AclFileAttributeView.class);

        UserPrincipalLookupService upls = file.getFileSystem().getUserPrincipalLookupService();
        UserPrincipal user = upls.lookupPrincipalByName(System.getProperty("user.name"));
        AclEntry.Builder builder = AclEntry.newBuilder();
        builder.setPermissions(AclEntryPermission.EXECUTE);
        builder.setPrincipal(user);
        builder.setType(AclEntryType.DENY);
        aclAttr.setAcl(Collections.singletonList(builder.build()));

        FiniteAutomata<String> automataFakeInput = new FiniteAutomata<>(file.toFile());

        try {
            automataFakeInput.isAccessibleFile();
        } finally {
            builder = AclEntry.newBuilder();
            builder.setPermissions(AclEntryPermission.DELETE);
            builder.setPrincipal(user);
            builder.setType(AclEntryType.ALLOW);
            aclAttr.setAcl(Collections.singletonList(builder.build()));
        }
    }
}

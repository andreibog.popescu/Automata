package com.andrei.automata.listeners;

import com.racesoft.utils.behaviour.Observable;
import com.racesoft.utils.behaviour.Observer;

/**
 * Print to console observer
 * <p/>
 * Created by Popescu on 7/9/2014.
 */
public class PrintToConsole implements Observer {

    @Override
    public void update(Observable observable, Object o) {
        if (observable != null && o != null) {
            System.out.println(o.toString());
        }
    }
}

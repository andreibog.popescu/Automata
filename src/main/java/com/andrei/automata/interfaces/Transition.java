package com.andrei.automata.interfaces;

import java.io.Serializable;

/**
 * Transition interface
 */
public interface Transition extends Serializable {

    /**
     * Returns state according to symbol
     *
     * @return state
     */
    public State getNextState();

}

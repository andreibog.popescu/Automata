package com.andrei.automata.pushdown;

import com.andrei.automata.finite.FiniteAutomata;
import com.andrei.automata.interfaces.State;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Pushdown Automata Class
 *
 * Created by Popescu on 8/6/2014.
 */
public class PushdownAutomata<TYPE> extends FiniteAutomata {

    private final Map<String, PushdownState<TYPE>> states = new HashMap<>();
    private String stack;

    public PushdownAutomata(File inputPath) {
        super(inputPath);
    }

    /**
     * Adds transition from input string
     *
     * @param transLine String containing a transition
     */
    @Override
    protected void readTransition(String transLine) {
        String[] transition = transLine.split("->");

        addTransition(getState(transition[0].trim()), transition[1].trim(), transition[2].trim(), getState(transition[3].trim()), transition[4].trim());
    }

    /**
     * Returns state in set
     *
     * @return state set
     */
    @Override
    public State getState(String stateName) {
        if (stateName != null && states.containsKey(stateName))
            return states.get(stateName);
        return new PushdownState<>("Invalid", false, false);
    }

    /**
     * Add state to set
     *
     * @param stateName input state
     */
    @Override
    public void addState(String stateName) {
        PushdownState tempState = null;

        if (stateName.endsWith("sf"))
            tempState = new PushdownState<>(stateName.substring(0, stateName.lastIndexOf("sf")), true, true);
        else {
            if (stateName.endsWith("s"))
                tempState = new PushdownState<>(stateName.substring(0, stateName.lastIndexOf("s")), true, false);
            if (stateName.endsWith("f")) {
                tempState = new PushdownState<>(stateName.substring(0, stateName.lastIndexOf("f")), false, true);
            }
        }
        if (!stateName.endsWith("sf") && !stateName.endsWith("s") && !stateName.endsWith("f")) {
            tempState = new PushdownState<>(stateName, false, false);
        }

        states.put(tempState.getStateName(), tempState);
    }

    /**
     * Add transition to starting state (if starting state exists, final state exists, transition isn't a duplicate)
     *
     * @param startState   starting state
     * @param popFromStack stack symbol to pop
     * @param symbol       transition symbol
     * @param endState     ending state
     * @param pushToStack  stack symbol to push
     */
    public void addTransition(State startState, String popFromStack, Object symbol, State endState, String pushToStack) {

        if (checkStateExists(startState) && checkStateExists(endState)) {

            PushdownState state = states.get(startState.getStateName());
            PushdownTransition transition = new PushdownTransition<>((PushdownState) states.get(endState.getStateName()), symbol, popFromStack, pushToStack);

            if (!state.checkTransitionExists(transition))
                state.getTransitions().put(transition.hashCode(), transition);
        }
    }

    /**
     * Set stack to default value "Z"
     */
    private void resetStack() {
        this.stack = "Z";
    }

    /**
     * Check if input is accepted by automata
     *
     * @param input input word
     * @return true if accepted by automata, false if it isn't
     */
    @Override
    public boolean isAcceptedInput(Object input) {

        super.setValid(false);
        resetStack();

        states.entrySet()
                .stream()
                .filter(state -> !super.isValid() && state.getValue().isStartState())
                .forEach(state -> this.consumeInput(input.toString(), state.getValue()));

        return super.isValid();
    }

    /**
     * Consume input and set valid to true if input is accepted by automata
     * Works for PDA
     *
     * @param input        input to consume
     * @param currentState state
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void consumeInput(String input, State currentState) {
        if (!currentState.isInvalid()) {
            Map<String, PushdownTransition> tempTranstions;
            if (input.length() > 0)
                tempTranstions = currentState.findTransitions(input.charAt(0));
            else
                tempTranstions = currentState.findTransitions("*");
            for (Map.Entry<String, PushdownTransition> transition : tempTranstions.entrySet()) {
                if (input.length() > 0) {
                    if (manageStack(transition.getValue().popFromStack, transition.getValue().pushToStack)) {
                        if (transition.getValue().getSymbol() == "*") {
                            consumeInput(input, transition.getValue().getNextState());
                        } else
                            consumeInput(input.substring(1), transition.getValue().getNextState());
                    }
                } else if (stack.equals("Z")) {
                    if (!currentState.isFinalState())
                        consumeInput(input, transition.getValue().getNextState());
                    else
                        super.setValid(true);
                }
            }
        }
    }

    /**
     * Pushes or pops symbols to/from the pushdown automata stack
     *
     * @param popFromStack symbol to pop from stack
     * @param pushToStack  symbol to push to stack
     * @return true if stack was modified, false if it was not
     */
    private boolean manageStack(String popFromStack, String pushToStack) {
        if (stack.startsWith(popFromStack)) {
            stack = stack.substring(popFromStack.length());
            if (!pushToStack.equals("*"))
                stack = pushToStack + stack;
            return true;
        } else
            return false;
    }

    /**
     * Check if state exists in set already
     *
     * @param state input state
     * @return true if state exists, false if it doesn't
     */
    @Override
    public boolean checkStateExists(State state) {
        return states.containsValue(state);
    }

    /**
     * String format:
     *
     * Pushdown Automata
     * States & Transitions
     *
     * <states>
     *
     * @return formatted string
     */
    @Override
    public String toString() {
        if (states.isEmpty()) return "No Automata Built. Please add states.";
        StringBuilder returnStr = new StringBuilder("\nPushdown Automata\nStates & Transitions\n\n");
        for (Map.Entry<String, PushdownState<TYPE>> state : states.entrySet())
            returnStr.append(state.getValue()).append("\n");
        return returnStr.toString();
    }
}

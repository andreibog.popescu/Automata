package com.andrei.automata.pushdown;

import com.andrei.automata.finite.FiniteTransition;
import com.andrei.automata.interfaces.State;

/**
 * Pushdown Transition
 *
 * Created by Popescu on 8/6/2014.
 */
public class PushdownTransition<TYPE> extends FiniteTransition {

    String popFromStack, pushToStack;
    private PushdownState endState;

    public PushdownTransition(PushdownState endState, TYPE symbol, String popFromStack, String pushToStack) {
        super(symbol);
        this.endState = endState;
        this.popFromStack = popFromStack;
        this.pushToStack = pushToStack;
    }

    /**
     * Returns next state
     *
     * @return state
     */
    @Override
    public State getNextState() {
        return endState;
    }

    /**
     * Returns if input is the same name as prezent state
     *
     * @param obj input name
     * @return true if both state have the same name, false if they don't
     */
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass()) {
            PushdownTransition transition = (PushdownTransition) obj;
            return transition.hashCode() == this.hashCode();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.endState.hashCode() * super.getSymbol().hashCode() * this.popFromStack.hashCode() * this.pushToStack.hashCode();
    }

    /**
     * String format: "with <symbol> -> State: <State> where <popFromStack> is replaced by <pushToStack>"
     *
     * @return formatted string
     */
    @Override
    public String toString() {
        return "with " + super.getSymbol() + " -> State: " + this.endState.getStateName() + " where " + this.popFromStack + " is replaced by " + this.pushToStack;
    }
}

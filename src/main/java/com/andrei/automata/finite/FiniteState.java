package com.andrei.automata.finite;

import com.andrei.automata.interfaces.State;
import com.andrei.automata.interfaces.Transition;

import java.util.HashMap;
import java.util.Map;

/**
 * Finite State
 *
 * Created by Popescu on 7/9/2014.
 */
public class FiniteState<TYPE> implements State {

    private final Map<String, FiniteTransition> transitions = new HashMap<>();
    private boolean start = false, fin = false;
    private String stateName;

    public FiniteState(TYPE stateName, Boolean start, Boolean fin) {
        this.start = start;
        this.fin = fin;
        this.stateName = stateName.toString();
    }

    /**
     * Returns state name
     */
    @Override
    public String getStateName() {
        return stateName;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Returns True if starting state, false if it isn't
     */
    @Override
    public boolean isStartState() {
        return start;
    }

    /**
     * Returns True if final state, false if it isn't
     */
    @Override
    public boolean isFinalState() {
        return fin;
    }

    /**
     * Returns true if stateName is "Invalid", false if it isn't
     */
    @Override
    public boolean isInvalid() {
        return this.stateName.equals("Invalid");
    }

    /**
     * Returns if input is the same name as prezent state
     *
     * @param obj input name
     * @return true if both state have the same name, false if they don't
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass()) {
            FiniteState state = (FiniteState) obj;
            return state.stateName.equals(this.stateName);
        }
        return false;
    }

    /**
     * Returns transitions for state
     *
     * @return transitions set
     */
    public Map<String, FiniteTransition> getTransitions() {
        return transitions;
    }

    /**
     * Returns transition from state
     *
     * @param inputSymbol input symbol
     * @return transition with symbol
     */
    @Override
    public Map<String, FiniteTransition> findTransitions(Object inputSymbol) {

        Map<String, FiniteTransition> tempTranstions = new HashMap<>();
        if (inputSymbol != null)
            transitions.entrySet()
                    .stream()
                    .filter(transition -> inputSymbol.hashCode() == transition.getValue().getSymbol().hashCode() ||
                            transition.getValue().getSymbol() == "*")
                    .forEach(transition -> tempTranstions.put(String.valueOf(transition.getValue().hashCode()), transition.getValue()));
        if (tempTranstions.isEmpty())
            tempTranstions.put("Invalid", new FiniteTransition<>(new FiniteState<>("Invalid", false, false), ""));
        return tempTranstions;
    }

    /**
     * Checks if transition exists already in set
     *
     * @param transition input transition
     * @return true if it exists already, false if it doesn't
     */
    @Override
    public boolean checkTransitionExists(Transition transition) {
        return transitions.containsValue(transition);
    }

    /**
     * String format:
     * <p/>
     * State: <stateName> (start)|(final)|blank
     * <transitions>
     *
     * @return formatted string
     */
    @Override
    public String toString() {
        StringBuilder returnStr = new StringBuilder("State: " + stateName);
        if (isStartState()) returnStr.append(" (start) ");
        if (isFinalState()) returnStr.append(" (final) ");
        for (Map.Entry<String, FiniteTransition> transition : transitions.entrySet())
            returnStr.append("\n").append(transition.getValue());
        return returnStr.toString();
    }
}

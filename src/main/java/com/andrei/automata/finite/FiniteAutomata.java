package com.andrei.automata.finite;

import com.andrei.automata.interfaces.Automata;
import com.andrei.automata.interfaces.State;
import com.racesoft.utils.behaviour.Observable;
import com.racesoft.utils.behaviour.Observer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.attribute.AclEntry;
import java.nio.file.attribute.AclEntryPermission;
import java.nio.file.attribute.AclEntryType;
import java.nio.file.attribute.AclFileAttributeView;
import java.util.*;

/**
 * Finite Automata
 *
 * Created by Popescu on 7/9/2014.
 */
public class FiniteAutomata<TYPE> implements Automata, Observable {

    private final Set<Observer> observers = new HashSet<>();
    private final Map<String, FiniteState<TYPE>> states = new HashMap<>();
    private File inputPath;
    private boolean valid;

    public FiniteAutomata(File inputPath) {
        this.inputPath = inputPath;
    }

    /**
     * Builds Finite automata
     *
     * @throws IOException if file isn't accessible
     */
    public void build() throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputPath), "UTF-8"))) {
            String line;
            if ((line = reader.readLine()) != null)
                this.readStates(line);
            else
                throw new IOException("Invalid input.");

            while ((line = reader.readLine()) != null) {
                this.readTransition(line);
            }
        }
    }

    /**
     * Check if file is accessible
     *
     * @return true if accessible, false if it isn't
     */
    public boolean isAccessibleFile() throws NullPointerException, IOException {
        if (inputPath == null)
            throw new NullPointerException("File is not accessible.");

        AclFileAttributeView aclAttr = Files.getFileAttributeView(inputPath.toPath(), AclFileAttributeView.class);
        List<AclEntry> aclEntryList = aclAttr.getAcl();

        for (AclEntry entry : aclEntryList)
            if (entry.type() == AclEntryType.DENY && entry.permissions().contains(AclEntryPermission.EXECUTE))
                throw new IOException("File is not accessible.");

        System.out.printf("File '" + inputPath.toString() + "' accessed successfully!\n");
        return true;
    }

    /**
     * Read states from input string and adds them to the DFA automata
     *
     * @param stateLine String containing states
     */
    private void readStates(String stateLine) {
        String[] states = stateLine.split(" ");

        for (String state : states) {
            addState(state);
        }
    }

    /**
     * Adds transition from input string
     *
     * @param transLine String containing a transition
     */
    protected void readTransition(String transLine) {
        String[] transition = transLine.split("->");

        addTransition(getState(transition[0].trim()), getState(transition[2].trim()), transition[1].trim());
    }

    /**
     * Check if input is accepted by automata
     *
     * @param input input word
     * @return true if accepted by automata, false if it isn't
     */
    @Override
    public boolean isAcceptedInput(Object input) {

        valid = false;

        states.entrySet()
                .stream()
                .filter(state -> !valid && state.getValue().isStartState())
                .forEach(state -> consumeInput(input.toString(), state.getValue()));

        return valid;
    }

    public boolean isValid() {
        return valid;
    }

    protected void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * Consume input and set valid to true if input is accepted by automata
     * Works for DFA, NFA & L(*)-NFA
     *
     * @param input             input to consume
     * @param currentState      state
     */
    @SuppressWarnings("unchecked")
    protected void consumeInput(String input, State currentState) {

        if (!currentState.isInvalid()) {
            Map<String, FiniteTransition> tempTranstions;
            if (input.length() > 0)
                tempTranstions = currentState.findTransitions(input.charAt(0));
            else
                tempTranstions = currentState.findTransitions("*");
            for (Map.Entry<String, FiniteTransition> transition : tempTranstions.entrySet()) {
                if (input.length() > 0) {
                    if (transition.getValue().getSymbol() == "*") {
                        consumeInput(input, transition.getValue().getNextState());
                    } else
                        consumeInput(input.substring(1), transition.getValue().getNextState());

                } else {
                    if (!currentState.isFinalState())
                        consumeInput(input, transition.getValue().getNextState());
                    else
                        setValid(true);
                }
            }
        }
    }

    /**
     * Returns state in set
     *
     * @return state set
     */
    public State getState(String stateName) {
        if (stateName != null && states.containsKey(stateName))
            return states.get(stateName);
        return new FiniteState<>("Invalid", false, false);
    }

    /**
     * Add state to set
     *
     * @param stateName     input state
     */
    @Override
    public void addState(String stateName) {
        FiniteState tempState = null;

        if (stateName.endsWith("sf"))
            tempState = new FiniteState<>(stateName.substring(0, stateName.lastIndexOf("sf")), true, true);
        else {
            if (stateName.endsWith("s"))
                tempState = new FiniteState<>(stateName.substring(0, stateName.lastIndexOf("s")), true, false);
            if (stateName.endsWith("f")) {
                tempState = new FiniteState<>(stateName.substring(0, stateName.lastIndexOf("f")), false, true);
            }
        }
        if (!stateName.endsWith("sf") && !stateName.endsWith("s") && !stateName.endsWith("f")) {
            tempState = new FiniteState<>(stateName, false, false);
        }

        states.put(tempState.getStateName(), tempState);
    }

    /**
     * Check if state exists in set already
     *
     * @param state     input state
     * @return true if state exists, false if it doesn't
     */
    @Override
    public boolean checkStateExists(State state) {
        return states.containsValue(state);
    }

    /**
     * Add transition to starting state (if starting state exists, final state exists, transition isn't a duplicate)
     *
     * @param startState    starting state
     * @param endState      ending state
     * @param symbol        transition symbol
     */
    @Override
    public void addTransition(State startState, State endState, Object symbol) {

        if (checkStateExists(startState) && checkStateExists(endState)) {

            FiniteState state = states.get(startState.getStateName());
            FiniteTransition transition = new FiniteTransition<>((FiniteState) states.get(endState.getStateName()), symbol);

            if (!state.checkTransitionExists(transition))
                state.getTransitions().put(transition.hashCode(), transition);
        }
    }

    @Override
    public Set<Observer> getObservers() {
        return observers;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    /**
     * String format:
     *
     * Finite Automata
     * States & Transitions
     *
     * <states>
     *
     * @return formatted string
     */
    @Override
    public String toString() {
        if (states.isEmpty()) return "No Automata Built. Please add states.";
        StringBuilder returnStr = new StringBuilder("\nFinite Automata\nStates & Transitions\n\n");
        for (Map.Entry<String, FiniteState<TYPE>> state : states.entrySet())
            returnStr.append(state.getValue()).append("\n");
        return returnStr.toString();
    }
}
package com.andrei.automata.finite;

import com.andrei.automata.interfaces.State;
import com.andrei.automata.interfaces.Transition;

/**
 * Finite Transition
 * <p/>
 * Created by Popescu on 7/9/2014.
 */
public class FiniteTransition<TYPE> implements Transition {

    private FiniteState endState;
    private TYPE symbol;

    public FiniteTransition(FiniteState endState, TYPE symbol) {
        this.endState = endState;
        this.symbol = symbol;
    }

    public FiniteTransition(TYPE symbol) {
        this.symbol = symbol;
    }

    /**
     * Returns next state
     *
     * @return state
     */
    @Override
    public State getNextState() {
        return endState;
    }

    /**
     * Returns if input is the same name as prezent state
     *
     * @param obj input name
     * @return true if both state have the same name, false if they don't
     */
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass()) {
            FiniteTransition transition = (FiniteTransition) obj;
            return transition.hashCode() == this.hashCode();
        }
        return false;
    }

    public TYPE getSymbol() {
        return symbol;
    }

    @Override
    public int hashCode() {
        return endState.hashCode() * symbol.hashCode();
    }

    /**
     * String format: "with <symbol> -> State: <State>"
     *
     * @return formatted string
     */
    @Override
    public String toString() {
        return "with " + symbol + " -> State: " + endState.getStateName();
    }
}
